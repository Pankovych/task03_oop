package com.epam.passive.view;

@FunctionalInterface
public interface Printable {

    void print();
}
