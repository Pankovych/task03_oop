package com.epam.passive.view;

import com.epam.passive.controller.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print GooListList");
        menu.put("2", "  2 - add Goods to list");
        menu.put("3", "  3 - search by");
        menu.put("4", "  4 - search by price");
        menu.put("5", "  5 - search by type");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        controller.getListGoods();
    }

    private void pressButton2() {
        System.out.println("Please input type of goods:");
        String type = input.nextLine();
        System.out.println("Please input producer of goods:");
        String producer = input.nextLine();
        System.out.println("Please input description of goods:");
        String description = input.nextLine();
        System.out.println("Please input price of goods:");
        double price = input.nextDouble();
        controller.addGoods(type, producer, price, description);
    }

    private void pressButton3() {
        controller.searchBy();
    }

    private void pressButton4() {
        System.out.println("Please input price:");
        double price = input.nextDouble();
        controller.searchByPrice(price);
    }

    private void pressButton5() {
        System.out.println("Please input type:");
        String type = input.nextLine();
        controller.searchByType(type);
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
