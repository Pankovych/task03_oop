package com.epam.passive.model;

import java.util.List;
import java.util.LinkedList;
import java.util.Scanner;

public class BusinessLogic implements Model {

    private List<Goods> goods;
    private static Scanner input = new Scanner(System.in);

    public BusinessLogic() {
        goods = new LinkedList<>();
        goods.add(new Goods("door", "doorDeveloper", 1000, "Simple door"));
        goods.add(new Goods("bath", "bathDeveloper", 3000, "Simple bath"));
        goods.add(new Goods("bath", "bathDeveloper", 2000, "Expensive bath"));
        goods.add(new Goods("bath", "bathDeveloper", 5000, "Just bath"));
    }

    @Override
    public final List<Goods> getListGoods() {
        return goods;
    }

    @Override
    public final void addGoods(final String type, final String producer, final double price, final String description) {
        goods.add(new Goods(type, producer, price, description));
    }

    @Override
    public final void searchBy() {
        System.out.println("1-by price");
        System.out.println("2-by type");
        System.out.println("3-by producer");
        int number = input.nextInt();
        if (number == 1) {
            System.out.println("Input price");
            double price = input.nextDouble();
            searchByPrice(price);
        } else if (number == 2) {
            for (Goods n : goods) {
                System.out.println(n.getType());
            }
            System.out.println("Input type");
            String type = input.nextLine();
            searchByType(type);
        } else if (number == 3) {
            for (Goods n : goods) {
                System.out.println(n.getProducer());
            }
            System.out.println("Input producer");
            String producer = input.nextLine();
            searchByProducer(producer);
        } else {
            System.out.println("Error: command not found");
        }
    }

    @Override
    public final void searchByPrice(final double price) {
        for (Goods n : goods) {
            if (n.getPrice() > price) {
                System.out.println(n.getType() + " " + n.getPrice() + " " + n.getProducer());
            }
        }
    }

    @Override
    public final void searchByType(final String type) {
        for (Goods n : goods) {
            if (n.getType() == type) {
                System.out.println(n.getType() + " " + n.getPrice() + " " + n.getProducer());
            }
        }
    }

    @Override
    public final void searchByProducer(final String producer) {
        for (Goods n : goods) {
            if (n.getProducer() == producer) {
                System.out.println(n.getType() + " " + n.getPrice() + " " + n.getProducer());
            }
        }
    }
}
