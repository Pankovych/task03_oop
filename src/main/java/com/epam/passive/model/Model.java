package com.epam.passive.model;

import java.util.List;

public interface Model {

    void addGoods(String type, String producer, double price, String description);

    List<Goods> getListGoods();

    void searchByPrice(double price);

    void searchByType(String type);

    void searchByProducer(String producer);

    void searchBy();

}
