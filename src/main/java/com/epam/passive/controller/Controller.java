package com.epam.passive.controller;

import com.epam.passive.model.*;

public interface Controller {
    void getListGoods();

    void addGoods(String type, String producer, double price, String description);

    void searchBy();

    void searchByPrice(double price);

    void searchByType(String type);

    void searchByProducer(String producer);
}
