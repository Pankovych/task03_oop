package com.epam.passive.controller;

import com.epam.passive.model.*;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public final void getListGoods() {
        List<Goods> newList = model.getListGoods();
        for (Goods n : newList) {
            System.out.println(n.getPrice() + " " + n.getType() + " " + n.getProducer());
        }
    }

    @Override
    public final void addGoods(final String type, final String producer, final double price, final String description) {
        model.addGoods(type, producer, price, description);
    }

    @Override
    public final void searchBy() {
        model.searchBy();
    }

    @Override
    public final void searchByPrice(final double price) {
        model.searchByPrice(price);
    }

    @Override
    public final void searchByType(final String type) {
        model.searchByType(type);
    }

    @Override
    public final void searchByProducer(final String producer) {
        model.searchByProducer(producer);
    }
}
